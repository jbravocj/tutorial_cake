CREATE DATABASE cake_cms;
\c cake_cms

CREATE SEQUENCE users_seq
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE 
    START WITH 1
    NO CYCLE;

CREATE TABLE users(
	id INT NOT NULL DEFAULT nextval('users_seq'),
	email VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created DATE,
    modified DATE,
	CONSTRAINT pk_users PRIMARY KEY (id)
);

CREATE SEQUENCE articles_seq
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE 
    START WITH 1
    NO CYCLE;

CREATE TABLE articles (
    id INT NOT NULL DEFAULT nextval('articles_seq'),
    user_id INT NOT NULL,
    title VARCHAR(255) NOT NULL,
    slug VARCHAR(191) NOT NULL,
    body TEXT,
    published BOOLEAN DEFAULT FALSE,
    created DATE,
    modified DATE,
    UNIQUE (slug),
    CONSTRAINT pk_articles PRIMARY KEY (id)
);

CREATE SEQUENCE tags_seq
    INCREMENT BY 1
    MINVALUE 1
    NO MAXVALUE 
    START WITH 1
    NO CYCLE;

CREATE TABLE tags (
    id INT NOT NULL DEFAULT nextval('tags_seq'),
    title VARCHAR(191),
    created DATE,
    modified DATE,
    UNIQUE (title),
    CONSTRAINT pk_tags PRIMARY KEY (id)
);

CREATE TABLE articles_tags (
    article_id INT NOT NULL,
    tag_id INT NOT NULL,
    CONSTRAINT pk_articles_tags PRIMARY KEY (article_id, tag_id)
);


ALTER TABLE articles ADD CONSTRAINT fk_user_article FOREIGN KEY (user_id)
REFERENCES users (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE articles_tags ADD CONSTRAINT fk_article FOREIGN KEY (article_id)
REFERENCES articles (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;

ALTER TABLE articles_tags ADD CONSTRAINT fk_tag FOREIGN KEY (tag_id)
REFERENCES tags (id) MATCH FULL
ON DELETE NO ACTION ON UPDATE NO ACTION;


INSERT INTO users (email, password, created, modified)
VALUES('cakephp@example.com', 'sekret', current_timestamp, current_timestamp);

INSERT INTO articles (user_id, title, slug, body, published, created, modified)
VALUES(1, 'First Post', 'first-post', 'This is the first post.', true, current_timestamp, current_timestamp);

INSERT INTO articles (user_id, title, slug, body, published, created, modified)
VALUES
(1, 'Second Post', 'second-post', 'This is the second post.', true, now(), now());
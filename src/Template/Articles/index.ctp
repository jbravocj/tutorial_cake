<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
        <title>Posts</title>
    </head>
    <body>
        <h1>Articles</h1>
        <p><?= $this->Html->link("Add Article", ['action' => 'add']) ?></p>
        <table>
            <tr>
                <th>Title</th>
                <th>Created</th>
                <th>Action</th>
            </tr>

        <!-- Here's where we iterate through our $articles query object, printing out article info -->

        <?php foreach ($articles as $article): ?>
            <tr>
                <td>
                    <?= $this->Html->link($article->title, ['action' => 'view', $article->slug]) ?>
                </td>
                <td>
                    <?= $article->created->format(DATE_RFC850) ?>
                </td>
                <td>
                    <?= $this->Html->link('Edit', ['action' => 'edit', $article->slug]) ?>
                    <?= $this->Form->postLink(
                        'Delete',
                        ['action' => 'delete', $article->slug],
                        ['confirm' => '¿Estás seguro que deseas eliminar este artículo?'])
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </table>
    </body>
</html>